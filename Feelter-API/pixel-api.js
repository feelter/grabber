const express = require('express');
const router = express.Router();

const pixelService = require('./pixel-service');

router.get('/:client_Id(\\d+)/:product_Id(\\d+)/:attr', async (request, response) => {

    response.writeHead(200, { "Content-Type": "text/html" });
    response.end(`<html>Loading image with src=${request.params.client_Id}.png <br/><img src='/${request.params.client_Id}.png'/></html>`);

});

router.get('/:client_Id(\\d+).png', async (request, response) => {
    let ref_encoded = request.query.r;
    var atob = require('atob');
    let referer = atob(ref_encoded); // request.headers.referer;
    let {client_Id} = request.params;
    let key = `feelter:private:${client_Id}`;
    let ifExist = {};
    let ifencountered = false;

    let data = await pixelService.getPixelInfo(client_Id, key, ref_encoded, referer, ifExist, ifencountered).catch(error => {
        console.log(error)
    });
    response.json( data )
    
});

module.exports = router;