require("dotenv").config({ path: './prod/.env'});
// console.log(`./.env.${process.env.NODE_ENV}`);

let keycloak_config = {
    "clientId": process.env.RESOURCE,
    "bearerOnly": true,
    "serverUrl": process.env.AUTH_SERVER_URL,
    "realm": process.env.REALM,
    "credentials": {
        "secret": process.env.SECRET  
    }
};

module.exports = keycloak_config;