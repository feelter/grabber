const redis = require('redis');
const client = redis.createClient({
    host: 'localhost',
    port: 6379
});

client.on("error", function (error) {
    console.error(error);
});

let createNewClient = (key) => {
    console.log("New Client");
    client.set(`encountered:${key}`, true);
}

var AWS = require('aws-sdk'); //AWS connected
AWS.config.update({ region: 'us-west-2' });
var sqs = new AWS.SQS({ apiVersion: '2021-07-13' });

let redisFuncGetKey = async function (key) {

    return new Promise((resv, rej) => {
        client.get(key, (error, value) => {
            if (error) {
                rej(error);
            }
            resv(value);
        });
    });
};

let getPixelInfo = (client_Id, key, ref_encoded, referer, ifExist, ifencountered) => {

    return new Promise(async(resolve, reject) => {

        ifencountered = await redisFuncGetKey(`encountered:${key}`);
        ifExist = await redisFuncGetKey(key);

        if (!ifExist && !ifencountered) {
            resolve(createNewClient(key));
        } else {
            console.log(`Client ${client_Id} already registered`)
        }
      
        console.log(`ref-encoded: ${ref_encoded}`);
        console.log(`client-key: ${key}`);
        console.log(`referer: ${referer}`);

        const params = { 
            MessageGroupId: '5',
            MessageBody: JSON.stringify({
                ref_encoded: ref_encoded,
                referrer: referer,
                client_key: key,
                date: (new Date()).toISOString(),
            }),
            QueueUrl: "https://sqs.us-west-2.amazonaws.com/001431519265/productResolver.fifo",
        };
        sqs.sendMessage(params, (err, data) => { //Send the product data to the queue (productResolver)
            if (err) {
                console.log("Error", err);
            } else {
                console.log("Successfully added message", data.MessageId, "in the queue");
                console.log(data);
            }
        });
    });
}

module.exports = { getPixelInfo };


/*

Pixel:

var pixelFilter = new Image();
pixelFilter.src = "http://localhost:8080/local/pixel/1.png?r="+btoa(window.location.href);

*/