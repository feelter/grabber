const redis = require('redis');
const client = redis.createClient({
    host: 'localhost',
    port: 6379
});

client.on("error", function (error) {
    console.error(error);
});

let createNewClient = (clientID) => {
    console.log("New Client")
    // client.set(`feelter:private:${clientID}:settings:general`, "")
    // client.set(`feelter:private:${clientID}:widget`, "")

    // client.set(`feelter:private:${clientID}:settings:vrating`, "")
    // client.set(`feelter:private:${clientID}:products:all`, "")
    // client.set(`feelter:private:${clientID}:products:blacklist`, "")
    // client.set(`feelter:private:${clientID}:reviews:blacklist`, "")
}


// USER

let getCustomerInfo = (clientID) => {

    return new Promise((resolve, reject) => {

        client.exists(`feelter:private:${clientID}:settings:general`, function (err, reply) { //fn ln domain categories
            if (reply === 1) { //if client existing : send back to the Front data linked to that client    
                console.log("Old Client")
                client.hgetall(`feelter:private:${clientID}:settings:general`, function (err, data) {
                    if (err) {
                        reject(err)
                    }
                    if (data) {
                        resolve(data)
                    }
                });

                // client.hmget(`feelter:private:${clientID}:settings:vrating`);
                // client.hmget(`feelter:private:${clientID}:products:all`);
                // client.hmget(`feelter:private:${clientID}:products:blacklist`);
                // client.hmget(`feelter:private:${clientID}:reviews:blacklist `);

            } else { // if new client : add it to redis with defaults parameters (reik)
                createNewClient(clientID);
                resolve({});
            }
        });
    });
};

let setCustomerInfo = (clientInfo, clientID) => {

    return new Promise((resolve, reject) => {
        client.exists(`feelter:private:${clientID}:settings:general`, function (err, reply) { //fn ln domain categories
            // if (reply === 1) { //if client existing : post the new updates  
            Object.keys(clientInfo).forEach(function (key) {
                client.hset(`feelter:private:${clientID}:settings:general`, `${key}`, `${clientInfo[key]}`, (err, data) => {
                    if (err) {
                        reject(err)
                    }
                    if (data) {
                        resolve(data)
                    }
                });
            })
            // } 
        });
        // client.hset(`feelter:private:${request.params.clientID}:settings:vrating`)
        // client.hset(`feelter:private:${request.params.clientID}:products:all`)
        // client.hset(`feelter:private:${request.params.clientID}:products:blacklist`)
        // client.hset(`feelter:private:${request.params.clientID}:reviews:blacklist `)
    });
};


// WIDGET

let getWidgetInfo = (clientID) => {

    return new Promise((resolve, reject) => {

        client.exists(`feelter:private:${clientID}:settings:general`, function (err, reply) { //fn ln domain categories
            if (reply === 1) { //if client existing : send back to the Front data linked to that client    
                console.log("Old Client")
                client.hgetall(`feelter:private:${request.params.clientID}:widget`, function (err, data) {
                    if (err) {
                        reject(err)
                    }
                    if (data) {
                        resolve(data)
                    }
                });

                // client.hmget(`feelter:private:${clientID}:settings:vrating`);
                // client.hmget(`feelter:private:${clientID}:products:all`);
                // client.hmget(`feelter:private:${clientID}:products:blacklist`);
                // client.hmget(`feelter:private:${clientID}:reviews:blacklist `);

            } else { // if new client : add it to redis with defaults parameters (reik)
                reject(new Error("Client Id isn't registered"))
            }
        });
    });
};

let setWidgetInfo = (clientInfo, clientID, WidgetID) => {

    return new Promise((resolve, reject) => {
        client.exists(`feelter:private:${clientID}:settings:general`, function (err, reply) { //fn ln domain categories
            // if (reply === 1) { //if client existing : post the new updates  
            Object.keys(clientInfo).forEach(function (key) {
                client.hset(`feelter:private:${request.params.clientID}:widget:${request.params.widgetID}`, `${key}`, `${clientInfo[key]}`, (err, data) => {
                    if (err) {
                        reject(err)
                    }
                    if (data) {
                        resolve(data)
                    }
                });
            })
            // } 
        });
        // client.hset(`feelter:private:${request.params.clientID}:settings:vrating`)
        // client.hset(`feelter:private:${request.params.clientID}:products:all`)
        // client.hset(`feelter:private:${request.params.clientID}:products:blacklist`)
        // client.hset(`feelter:private:${request.params.clientID}:reviews:blacklist `)
    });
};

module.exports = { getCustomerInfo, setCustomerInfo, getWidgetInfo, setWidgetInfo };






