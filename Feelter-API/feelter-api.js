const express = require('express');
const router = express.Router();

const keycloak = require('./keycloak-config').initKeycloak();
router.use(keycloak.middleware());

const feelterApiService = require('./feelter-api-service');
const pixelService = require('./pixel-service');

// USER

router.get(`/settings/user/:clientID`, keycloak.protect(), async (request, response) => { //petParams //Front demande : Existe => Je lui donne / Existe pas => Je lui signifie et j'inscrit par defaut
    let clientID = request.params.clientID;
    let data = await feelterApiService.getCustomerInfo(clientID).catch(error => {
        console.log(error)
    });
    response.json({ data });

});

router.post('/settings/user/:clientID', keycloak.protect(), async (request, response) => { // Nouvelles infos du Front, je maj les sadots concernes // Promise avec loulaa object.keys
    let clientID = request.params.clientID;
    try {
        let postData = await feelterApiService.setCustomerInfo(request.body, clientID);
        response.json({ status: true, message: "Data saved with success !", });
    } catch (error) {
        response.status(400).json({ status: false, message: "Something went wrong.", data: error });
    }

});


// WIDGET

router.get(`/settings/widget/:clientID`, keycloak.protect('shay_admin'), async (request, response) => {
    let clientID = request.params.clientID;
    let data = await feelterApiService.getWidgetInfo(clientID).catch(error => {
        console.log(error)
    });
    response.json({ data })
    
});

router.post(`/:clientID/settings/widget/:widgetID>`, keycloak.protect('shay_admin'), async (request, response) => { // Nouvelles infos du Front, je maj les sadots concernes // Promise avec loulaa object.keys
    let clientID = request.params.clientID;
    let widgetID = request.params.widgetID;
    try {
        let postData = await feelterApiService.setCustomerInfo(request.body, clientID, widgetID);
        response.json({ status: true, message: "Data saved with success !", });
    } catch (error) {
        response.status(400).json({ status: false, message: "Something went wrong.", data: error });
    }

});


module.exports = router;

