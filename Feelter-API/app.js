const express = require('express');
const app = express();
const port = 8080;

require('dotenv').config()

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const feelterApi = require('./feelter-api'); // Target feelter api
const pixelApi = require('./pixel-api'); // Target Pixel

try {
    app.use("/api", feelterApi);
    app.use("/local/pixel", pixelApi);
} catch (err) {
    console.error(`Error connecting to API - ${err}`);
}

app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`)
})

