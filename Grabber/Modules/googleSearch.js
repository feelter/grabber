//Searchs Product on Google and retrieves results of the first page (without Config) 

const puppeteer = require('puppeteer');

let doSearch = async function (searchQueries) { //General Search
    searchQueries.forEach(searchQuery => {
        return searchGoogle(searchQuery);
    })
}

let doSearchAlt = async function (alt) { //Search by alt
    let resultAlt = searchGoogle(alt);
    return resultAlt;
}

let doSearchSrc = async function (src) { //Search by src
    const doSomething = (results) => {
        console.log(results)
        for (let i = 0; i < results.length; i++) {
            doSearchTitle(results[i].title);
        }
    }
    reverseImageSearch(src, doSomething);
}

let doSearchTitle = async function (title) { //Search by title
    let resultTitle = searchGoogle(title);
    return resultTitle;
}

let doSearchDesc = async function (description) { //Search by description
    let resultDesc = searchGoogle(description);
    return resultDesc;
}

let doSearchCategory = async function (category) { //Search by category
    let resultCategory = searchGoogle(category);
    return resultCategory;
}

let searchGoogle = async (searchQuery) => {

    try {

        const browser = await puppeteer.launch({
            headless: false, //To see process in live (Chromium)
            defaultViewport: null,
            args: ["--no-sandbox"],
        });

        const page = await browser.newPage();

        await page.goto('https://www.google.com'); //Google URL
        if (await page.waitForSelector('div[id="SIvCob"]')) { //For english results
            await page.$eval('div[id="SIvCob"] > a[dir="ltr"]', button => button.click());
        }
        await page.screenshot({ path: "screenShot1.png" });
        await page.type('input[name="q"]', searchQuery); //Search Bar
        await page.$eval('input[name=btnK]', button => button.click()); //Search Button
        await page.screenshot({ path: "screenShot2.png" });
        await page.waitForTimeout(8000);
        await page.waitForSelector('div[id=search]'); //Search Page
        await page.waitForTimeout(8000);

        const searchResults = await page.$$eval('div[id=rso]', results => {

            let data = []; //Array to hold all our results

            results.forEach(parent => { //Iterate over all the results

                const ele = parent.querySelector('h2'); //Check if parent has h2 with text 'Web Results'

                if (ele === null) { //If element with 'Web Results' Title is not found then continue to next element
                    return;
                }

                let gCount = parent.querySelectorAll('div[class=g]'); //Check if parent contains 1 div with class 'g' or contains many but nested in div with class 'srg'

                if (gCount.length === 0) { //If there is no div with class 'g' that means there must be a group of 'g's in class 'srg'

                    gCount = parent.querySelectorAll('div[class=srg] > div[class=g]'); //Targets all the divs with class 'g' stored in div with class 'srg'
                }

                gCount.forEach(result => { //Iterate over all the divs with class 'g'

                    const title = result?.querySelector('div[class=tF2Cxc] > div[class=yuRUbf] > a >  h3')?.innerText; //Target the title
                    const url = result?.querySelector('div[class=tF2Cxc] > div[class=yuRUbf] > a')?.href; //Target the url 
                    const description = result?.querySelector('div[class=tF2Cxc] > div[class=IsZvec] > span[class=aCOpRe] > span')?.innerText; //Target the description

                    data.push({ title, url, description });

                });

            });

            return data;

        });

        await browser.close();

        console.log(searchResults);

        return searchResults;

        // let trustedhosts = ['amazon'];// = externalservice.get trutesdsorces
        // for (let i = 0; i < searchResults.length; i++) {
        //     if (searchResults[i].url.includes(trustedhosts)) {
        //         console.log('Trusted Name: ', searchResults[i].title);
        //     }
        // }

    }
    catch (error) {
        console.log(`Extraction failure ${error}`);
    }

};

let tmp;

let searchTotal = async ({ alt, src, title }) => {
    if (alt != 'N/A' && (tmp = await doSearchAlt(alt)) != null) { //If alt: search by alt
        console.log(`1 ${tmp}`);
        return tmp;
    } else if (src && (tmp = await doSearchSrc(src)) != null) { //Else if src: search by src
        console.log(`2 ${tmp}`);
        return tmp;
    } else if (title && (tmp = await doSearchTitle(title)) != null) { //ELse if title: search by title
        console.log(tmp);
        return tmp;
    }
}


searchTotal({ title: "airpods 2" }) //Search for product which title is "airpods 2"
//doSearch(['chicken', 'dogs']);
//doSearchSrc('http://localhost:8000/product/wireless-audio-system-multiroom-360/');


module.exports = { doSearch, doSearchAlt, doSearchCategory, doSearchDesc, doSearchSrc, doSearchTitle, searchGoogle };
