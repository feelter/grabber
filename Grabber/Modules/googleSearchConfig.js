//Searchs Product on Google and retrieves results of the first page (with Config) 

const puppeteer = require('puppeteer');
const reverseImageSearch = require('reverse-image-search-google');

let doSearch = async function (searchQueries, config) { //General Search
    searchQueries.forEach(searchQuery => {
        return searchGoogle(searchQuery, config);
    })
}

let doSearchAlt = async function (alt, config) { //Search by alt
    let resultAlt = searchGoogle(alt, config);
    return resultAlt;
}

let doSearchSrc = async function (src, config) { //Search by src
    const doSomething = (results) => { //reverse-image and then research by titles (from the reverse-img)
        console.log(results)
        for (let i = 0; i < results.length; i++) {
            doSearchTitle(results[i].title, config);
        }
    }
    reverseImageSearch(src, doSomething);
}

let doSearchTitle = async function (title, config) { //Search by title
    let resultTitle = await searchGoogle(title, config);
    return resultTitle;
}

let doSearchDesc = async function (description, config) { //Search by description
    let resultDesc = searchGoogle(description, config);
    return resultDesc;
}

let doSearchCategory = async function (category, config) { //Search by category
    let resultCategory = searchGoogle(category, config);
    return resultCategory;
}

const googleSearchConfig = { //Config of all the selectors we need (Google)
    "googleUrl": 'https://www.google.com',
    "selectEn": 'div[id="SIvCob"]',
    "googleEn": 'div[id="SIvCob"] > a[dir="ltr"]',
    "searchBar": 'input[name="q"]',
    "searchButton": 'input[name=btnK]',
    "waitSearchPage": 'div[id=search]',

    "resultsPage": 'div[id=rso]',
    "gCount1": 'div[class=g]',
    "gCount2": 'div[class=srg] > div[class=g]',
    "title": 'div[class=tF2Cxc] > div[class=yuRUbf] > a >  h3',
    "url": 'div[class=tF2Cxc] > div[class=yuRUbf] > a',
    "description": 'div[class=tF2Cxc] > div[class=IsZvec] > span[class=aCOpRe] > span',
};


let searchGoogle = async (searchQuery, myConfig) => {

    try {

        const browser = await puppeteer.launch({
            headless: false,
            defaultViewport: null,
            args: ["--no-sandbox"],
        });

        const page = await browser.newPage();

        await page.goto(myConfig.googleUrl); // Google URL
        if (await page.waitForSelector(myConfig.selectEn)) { // For english results
            await page.$eval(myConfig.googleEn, button => button.click());
        }
        // await page.screenshot({ path: "screenShot1.png" }); 
        await page.type(myConfig.searchBar, searchQuery); // Search Bar
        await page.$eval(myConfig.searchButton, button => button.click()); // Search Button
        // await page.screenshot({ path: "screenShot2.png" }); 
        await page.waitForTimeout(8000);
        await page.waitForSelector(myConfig.waitSearchPage); // Search Page
        await page.waitForTimeout(8000);

        const searchResults = await page.$$eval(myConfig.resultsPage, (results, myConfig) => {

            let data = []; //Array to hold all our results

            results.forEach(parent => { //Iterate over all the results

                const ele = parent.querySelector('h2'); //Check if parent has h2 with text 'Web Results'

                if (ele === null) { //If element with 'Web Results' Title is not found  then continue to next element
                    return;
                }

                let gCount = parent.querySelectorAll(myConfig.gCount1); //Check if parent contains 1 div with class 'g' or contains many but nested in div with class 'srg'

                if (gCount.length === 0) { //If there is no div with class 'g' that means there must be a group of 'g's in class 'srg'

                    gCount = parent.querySelectorAll(myConfig.gCount2); //Targets all the divs with class 'g' stored in div with class 'srg'
                }

                gCount.forEach(result => { //Iterate over all the divs with class 'g'

                    const title = result?.querySelector(myConfig.title)?.innerText; //Target the title

                    const url = result?.querySelector(myConfig.url)?.href; //Target the url 

                    const description = result?.querySelector(myConfig.description)?.innerText; //Target the description

                    data.push({ title, url, description });

                });

            });

            return data;

        }, myConfig);

        await browser.close();

        console.log(searchResults);

        return searchResults;

    }
    catch (error) {
        console.log(`Extraction failure ${error}`)
    }

};


let searchTotal = async ({ alt, src, title }, config1) => {
    if (alt != 'N/A' && (await doSearchAlt(alt, config1)) != null) { //If alt: search by alt
        // console.log(await doSearchAlt(alt, config1));
        return await doSearchAlt(alt, config1);
    } else if (src && (await doSearchSrc(src, config1)) != null) { //Else if src: search by src
        // console.log(await doSearchSrc(src, config1));
        return await doSearchSrc(src, config1);
    } else if (title && (await doSearchSrc(title, config1)) != null) { //ELse if title: search by title
        // console.log(await doSearchSrc(src, config1));
        return await doSearchSrc(title, config1);
    }
}


searchTotal({ src: "https://images-na.ssl-images-amazon.com/images/I/61e8VDHwIHL._AC_SY355_.jpg" }, googleSearchConfig) //Search for product which this image source


module.exports = { googleSearchConfig, searchTotal, doSearch, doSearchAlt, doSearchCategory, doSearchDesc, doSearchSrc, doSearchTitle, searchGoogle };
