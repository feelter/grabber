//Searchs Product on Amazon and retrieves reviews of the first product (with Config) 

const { urlencoded } = require("express");
const fetch = require("node-fetch");

const puppeteer = require('puppeteer');
const $ = require('cheerio');
const mongoose = require("mongoose");

var AWS = require('aws-sdk'); //AWS connected
AWS.config.update({ region: 'us-west-2' });
var sqs = new AWS.SQS({ apiVersion: '2021-06-14' });
var ObjectId = require('mongodb').ObjectID;

// const mongoUrl = `mongodb+srv://feelter_shay:mongodb19@reviews.ianlx.mongodb.net/reviews` //My Mongo Link
const mongoUrl = "mongodb+srv://test:test@cluster0.mficn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority" //Mongo Link

mongoose.connect(mongoUrl, { //Connecting to Mongo
    useUnifiedTopology: true,
    useNewUrlParser: true,
}).then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err));

var amazonSearchConfig = { //Config of all the selectors we need (Amazon)
    "userAgent": `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.49`,
    "amazonUrl": 'https://www.amazon.com',
    "searchBar": 'input[name="field-keywords"]',
    "searchButton": 'input[id="nav-search-submit-button"]',
    "waitSearchPage": 'div[class="s-main-slot s-result-list s-search-results sg-row"]',
    "firstPdtReviews": 'span[class="a-size-base"]',
    "reviewsPage": 'a[class="a-link-emphasis a-text-bold"]',
    "waitReviewsPage": 'div[id="cm_cr-review_list"]',
    "reviewSection": 'div[class="a-section review aok-relative"]',
    "nextPage": 'li[class="a-last"] > a',

    "selectAvatar": 'div[class="a-profile-avatar"] > img',
    "selectAuthor": 'span[class="a-profile-name"]',
    "selectProfile": 'a[class="a-profile"]',
    "selectRating": 'span[class="a-icon-alt"]',
    "selectTitle": 'a[class="a-size-base a-link-normal review-title a-color-base review-title-content a-text-bold"] > span',
    "selectDate": 'span[class="a-size-base a-color-secondary review-date"]',
    "selectHelpful": 'span[class="a-size-base a-color-tertiary cr-vote-text"]',
    "selectReview": 'span[class="a-size-base review-text review-text-content"] > span',
    "selectReviewImg": 'img[class="review-image-tile"]',
};


let doSearch = function (searchQueries, product_id, config) { //Search Product
    return new Promise((resolve, reject) => {
        searchQueries.forEach(searchQuery => {
            resolve(searchAmazon(searchQuery, product_id, config));
        })
    })
}

let searchAmazon = async (searchQuery, myProduct_id, myConfig) => {

    let pageCount = 0;
    let MAX_PAGE = 2; // number of review pages returned
    let searchResultArr = [];
    let searchResults;

    try {

        const browser = await puppeteer.launch({
            headless: false, //False to watch process in live (Chromium)
            defaultViewport: null,
            args: ["--no-sandbox"],
        })

        const page = await browser.newPage();

        await page.setUserAgent(myConfig.userAgent);

        await page.goto(myConfig.amazonUrl); //Amazon URL
        await page.type(myConfig.searchBar, searchQuery); //Search Bar
        await page.$eval(myConfig.searchButton, button => button.click()); // Search Button

        await page.waitForSelector(myConfig.waitSearchPage); //Search Page
        await page.waitForTimeout(6000);
        // await page.screenshot({ path: "screenShot1.png" }); 

        await page.$eval(myConfig.firstPdtReviews, button => button.click()); //First Product Reviews
        await page.waitForTimeout(8000);
        // await page.screenshot({ path: "screenShot2.png" });

        await page.$eval(myConfig.reviewsPage, button => button.click()); //Reviews Page
        await page.waitForTimeout(4000);

        await page.waitForTimeout(4000);

        while (await page.waitForSelector(myConfig.waitReviewsPage) && pageCount < MAX_PAGE) {

            searchResults = await page.$$eval(myConfig.reviewSection, (results, myConfig) => { //Review Section 

                let data = []; //Array to hold all our reviews

                results.forEach(result => { //Iterate over all the reviews

                    const website = myConfig.amazonUrl.replace('https://www.', ''); //Retrieve website name
                    const avatar = result?.querySelector(myConfig.selectAvatar)?.src; //Retrieve avatar
                    const author = result?.querySelector(myConfig.selectAuthor)?.innerText; //Retrieve author
                    const profile = result?.querySelector(myConfig.selectProfile)?.href; //Retrieve profile
                    const rating = result?.querySelector(myConfig.selectRating)?.innerText.match((/\d+\.\d+|\d+\b|\d+(?=\w)/g) || []).map(function (v) { return +v; }).shift(); //Retrieve rating (only the number)
                    const title = result?.querySelector(myConfig.selectTitle)?.innerText; //Retrieve title
                    const date = result?.querySelector(myConfig.selectDate)?.innerText.replace('Reviewed in the United States on ', ''); //Retrieve date            
                    const helpful = result?.querySelector(myConfig.selectHelpful)?.innerText; //Retrieve helpful section
                    const review = result?.querySelector(myConfig.selectReview)?.innerText; //Retrieve review
                    const reviewImg = result?.querySelector(myConfig.selectReviewImg)?.src; //Retrieve review pictures if existing

                    data.push({ website, avatar, author, profile, rating, title, date, helpful, review, reviewImg });
                });

                return data;

            }, myConfig);

            console.log(`---------------------------start of page ${pageCount + 1}-------------------------------------------------------`);
            console.log(searchResults); //Display Reviews
            console.log(`----------------------------end of page ${pageCount + 1}--------------------------------------------------------`);

            let newDate = function (date) {
                return new Date(date)
            }

            var MongoClient = require('mongodb').MongoClient;

            MongoClient.connect(mongoUrl, function (err, db) { //Send automatically the reviews to MongoDB
                if (err) throw err;
                var dbo = db.db("amazon"); //Connect to database "amazon"
                for (let i = 0; i < searchResults.length; i++) {
                    var myobj = {
                        productID: myProduct_id,
                        website: searchResults[i].website,
                        avatar: searchResults[i].avatar,
                        author: searchResults[i].author,
                        profile: searchResults[i].profile,
                        rating: searchResults[i].rating,
                        title: searchResults[i].title,
                        date: newDate(searchResults[i].date), 
                        helpful: searchResults[i].helpful,
                        review: searchResults[i].review,
                        reviewImg: searchResults[i].reviewImg,
                    };
                    dbo.collection("reviews_list").insertOne(myobj, function (err, res) { //Sent to collection "reviews_list"
                        if (err) throw err;
                        console.log("1 document inserted");
                        db.close();
                    });
                }
            });
            // Collection URL: https://cloud.mongodb.com/v2/60a36f9532e41830bc48943a#metrics/replicaSet/60a3715527c38868b3fb7664/explorer/reviews/amazonReviews/find (Shay Ids)

            searchResultArr[pageCount] = searchResults;
            pageCount++;

            try {
                await page.$eval(myConfig.nextPage, button => button.click()); //Next Page (Reviews)
            }
            catch (err) {
                break;
            }
            await page.waitForTimeout(4000);
            // await page.screenshot({ path: "screenShot4.png" })    
        }
        await browser.close();

        return searchResults;
    }
    catch (error) {
        console.log(error)
    }

};

let resShow = async function () { 

    var queueURL = "https://sqs.us-west-2.amazonaws.com/001431519265/reviewsAmazon.fifo";

    var params = {
        AttributeNames: [
            "SentTimestamp"
        ],
        MaxNumberOfMessages: 1,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: queueURL,
        VisibilityTimeout: 360,
        WaitTimeSeconds: 0
    };

    while (true) { 
        await new Promise(resolve => setTimeout(resolve, 60000));
        sqs.receiveMessage(params, async function (err, data) { //Get the product id from the queue (amazonReviews)
            if (err) {
                console.log("Receive Error", err);
            } else if (data.Messages) {

                const myJSON = data.Messages[0].Body;
                const myObj = JSON.parse(myJSON);

                let pdtID = myObj.product_ID;
                console.log("Message", pdtID);

                var MongoClient = require('mongodb').MongoClient;

                MongoClient.connect(mongoUrl, function (err, db) { //Send automatically the product to MongoDB
                    if (err) throw err;
                    var dbo = db.db("products");  //Connect to database "products"
                    dbo.collection("products_list").findOne({ _id: ObjectId(`${myObj.product_ID}`) }, async function (err, result) { //Sent to collection "products_list"
                        if (err) throw err;
                        console.log(result.PRODUCT_NAME);
                        let pdt_name = result.PRODUCT_NAME;
                        let pdt_ID1 = result._id;
                        let pdt_ID = pdt_ID1.toString();
                        return new Promise((resolve, reject) => {
                            resolve(doSearch([pdt_name], pdt_ID, amazonSearchConfig));
                            db.close();
                        })
                    });
                });

                var deleteParams = { 
                    QueueUrl: queueURL,
                    ReceiptHandle: data.Messages[0].ReceiptHandle
                };
                sqs.deleteMessage(deleteParams, async function (err, data) { //Delete the message in the queue after retrieving the product id
                    if (err) {
                        console.log("Delete Error", err);
                    } else {
                        console.log("Message Deleted", data);
                    }
                });
            }
        });
    };
}

resShow();

module.exports = { doSearch, searchAmazon, amazonSearchConfig };
