const fetch = require("node-fetch");
var encodeUrl = require('encodeurl');
const request = require("request");

const mongoose = require("mongoose");
const mongoUrl = "mongodb+srv://test:test@cluster0.mficn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority" //Mongo Link

var AWS = require('aws-sdk'); //AWS connected
AWS.config.update({ region: 'us-west-2' });
var sqs = new AWS.SQS({ apiVersion: '2021-06-14' });
var ObjectId = require('mongodb').ObjectID;

mongoose.connect(mongoUrl, { //Connecting to Mongo
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then(() => console.log('MongoDB connected...'))
  .catch(err => console.log(err));


  let normalize = function(str) { // Check with Yair
    let firstStep = str.toLowerCase()
    let secondStep = firstStep.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");
    let lastStep = "";
    let limitStr = str.split(' ').length;
    for (let i = 0; i < limitStr && i < 4; i++) {
      lastStep += secondStep.split(" ")[i];
      // console.log(limitStr)
      if (i <= limitStr - 2 && i < 3) {
              lastStep += " ";
      }
    }
    console.log(lastStep)
    return lastStep
}


let updateProductInfo = async function (productId, productName) { //Function to update the product (in Mongo) with the oldSystem info

  let q = productName;
  let productTitle = productName;

  let feelter_next = `https://json.feelter.com/?q=${q}&rhost=feelter-next.com&productTitle=${productTitle}&scriptType=carousel`; //OldSystem URL (switching product name)

  let encoded = encodeUrl(feelter_next);
  console.log("URL: ", encoded); //Encoded URL

  request(feelter_next, (err, resp, data) => {

    let data2 = JSON.parse(data);

    console.log("Exist ? ", data2.hasOwnProperty("Error")) //Check if the product exist in the old system

    if (data2.hasOwnProperty("Error")) { //If the product doesn't exist in the old system
      console.log("Couldn't find product: ", productName, " in the old system")
    } else { //If the product exist in the old system

      let mydata = data2[0][productName];

      const computed = mydata.Computed;
      const priority = mydata.Priority;
      const sentiment = mydata.Sentiment;
      const industryID = mydata.IndustryID;
      const phrase = mydata.Phrase;
      const gender = mydata.Gender;
      const meta = mydata.Meta;
      const sentimentDistribution = mydata.SentimentDistribution;
      const aspectsCounters = mydata.AspectsCounters;

      var MongoClient = require('mongodb').MongoClient;

      MongoClient.connect(mongoUrl, function (err, db) {
        if (err) throw err;
        var dbo = db.db("products"); //Connect to database "products"
        var myquery = { _id: ObjectId(productId) };
        var myobj = {
          $set: {
            oldSystemFt: {
              Computed: computed,
              Priority: priority,
              Sentiment: sentiment,
              Industry_ID: industryID,
              Phrase: phrase,
              Gender: gender,
              Meta: meta,
              Sentiment_Distribution: sentimentDistribution,
              Aspects_Counters: aspectsCounters,
            }

          }
        };
        dbo.collection("products_list").updateOne(myquery, myobj, function (err, res) { //Update automatically the product concerned in MongoDB
          if (err) throw err;
          console.log("1 document updated");
          db.close();
        });
      })
    }
  })
}

let oldSystemReviews = async function (productId, productName) { //Function to send the reviews (in Mongo) from the oldSystem info

  try {

    let q = productName;
    let productTitle = productName;

    let feelter_next = `https://json.feelter.com/?q=${q}&rhost=feelter-next.com&productTitle=${productTitle}&scriptType=carousel`; //OldSystem URL (switching product name)
console.log("Lien URL", feelter_next)
    request(feelter_next, productId, (err, resp, data) => {

      console.log(`Product ID: ${productId}`);
      console.log("Product Name: ", productName);

      let data2 = JSON.parse(data);

      if (data2.hasOwnProperty("Error")) { //If the product doesn't exist in the old system
        console.log("Couldn't find product: ", productName, " in the old system")
      } else { //If the product exist in the old system

        let data3 = data2[0][productName]["Data"]; //Target the "Data" section
        console.log("Keys", Object.keys(data3)); //Display the keys of each review

        Object.keys(data3).forEach(function (key) { //loop on those keys to return all the reviews

          let elmt = data3[key];

          const websitesList = { "1": "Twitter", "2": "Facebook Graph", "3": "TripAdvisor", "4": "Instagram", "5": "GooglePlus", "6": "Vine", "7": "Tumblr", "8": "Orbitz", "9": "Yelp", "10": "Flickr", "11": "Pinterest", "12": "Expedia", "13": "Booking", "14": "JohnLewis", "15": "YouTube", "16": "Engadget", "17": "Bestbuy", "18": "BHPhotoVideo", "19": "Newegg", "20": "Amazon", "21": "GameStop", "22": "ToysRUs", "23": "zZounds", "24": "TripAdvisor Photos", "25": "GooglePlus Photos", "26": "CruiseCritic", "27": "Foursquare Tips", "28": "Foursquare Photos", "29": "Cnet", "30": "Pro Engadget", "31": "Facebook Googled", "32": "Facebook Reviews", "33": "Google Reviews - OLD", "34": "HolidayCheck", "35": "Agoda", "36": "Hotels", "37": "PhotoBucket", "38": "WordPress", "39": "AlphaRooms", "40": "Venere", "41": "HolidayWatchdog", "42": "HostelWorld", "43": "LateRooms", "44": "Drugs", "45": "EverydayHealth", "46": "Reddit", "47": "MakeupAlley", "48": "Fragrantica", "49": "Overstock", "51": "SkinStore", "50": "FragranceX", "52": "AcneOrg", "53": "VitaminShoppe", "54": "HollandAndBarrett", "55": "BodyBuilding", "56": "A1Supplements", "57": "Vitacost", "58": "Epinions", "59": "Adorama", "60": "MetaCritic", "61": "Pet360", "62": "PetFlow", "63": "Wag", "64": "MichelinTravel", "65": "Viator", "66": "GoodReads", "67": "SonicElectronix", "68": "Crutchfield", "69": "DrugStore", "70": "IMDb", "71": "Reevoo", "72": "SamAsh", "73": "Tesco", "74": "Influenster", "75": "OpticsPlanet", "76": "PhotographyReview", "77": "Argos", "78": "OutdoorGearLab", "79": "TrailSpace", "80": "CampMor", "81": "Shoes", "82": "WhatCar", "83": "Edmunds", "84": "KelleyBlueBook", "85": "SupplementReviews", "86": "ProductReview", "87": "ZooPlus", "88": "AllAboutDogFood", "89": "Facebook Search", "90": "YellowPages", "91": "Cars", "92": "TheCarConnection", "93": "Vimeo", "94": "WotIf", "95": "Google Reviews", "96": "TotalBeauty", "97": "Nordstrom", "98": "Debenhams", "99": "SlideShare", "100": "FootLocker", "101": "FinishLine", "102": "RunRepeat", "103": "Zappos", "104": "JCPenney", "105": "DSW", "106": "FootAsylum", "107": "Schuh", "108": "BestBuy Canada", "109": "Sephora Community", "110": "Sephora Gallery", "111": "Sephora", "112": "Boots", "113": "MarksAndSpencer", "114": "SpaceNK", "115": "LookFantastic", "116": "FeelUnique", "117": "CarGurus", "118": "BeautyHeaven", "119": "AruKereso", "120": "EEVblog", "121": "TEquipment", "122": "Influenster Gallery", "123": "DUMMY SOURCE", "124": "BeautyBulletin", "125": "SampleRoom", "126": "FragranceNet", "127": "TheBay", "128": "Nykaa", "129": "ChickAdvisor", "130": "Dillards", "131": "BeautyReview", "132": "Allure", "133": "Yell", "134": "Trustpilot", "135": "AmazonUK", "136": "Etsy", "137": "Costco", "138": "LG", "139": "Samsung", "140": "ABT", "141": "Sony", "142": "RcWilley", "143": "BeautyBrands", "144": "UltaBeauty", "145": "AbsoluteSkin", "146": "Tiktok" };
          var keys = Object.keys(websitesList);
          let website;
          let websiteName = keys.map(key => {
            if (elmt.SourceID == key) {
              website = websitesList[key];
              return website;
            }
          });
          const dataKey = key;
          const avatar = elmt.Author.Avatar.Origin;
          const author = elmt.Author;
          const profile = elmt.Author.UserOrigin;
          let rating = elmt.Engagement.Rating;
          console.log("rating: ", rating)
          if (rating != null) {
            rating = elmt.Engagement.Rating.match((/\d+\.\d+|\d+\b|\d+(?=\w)/g) || []).map(function (v) { return +v; }).shift(); //Retrieve rating (only the number)
          }
          const title = elmt.Title;
          const date = elmt.Posted;
          const helpful = elmt.Helpful;
          const review = elmt.Text;
          const reviewImg = elmt.Media;
          const engagement = elmt.Engagement;
          const unhelpful = elmt.Unhelpful;

          let newDate = function (date) {
            return new Date(date)
          }

          var MongoClient = require('mongodb').MongoClient;

          MongoClient.connect(mongoUrl, function (err, db) { //Send automatically the product (trusted) name to MongoDB
            if (err) throw err;

            var dbo = db.db("oldSystem"); //Connect to database "oldSystem"

            var myobj = {
              product_ID: productId, //Retrieve Product ID
              dataKey: dataKey, //Retrieve Review Key
              website: website, //Retrieve website name
              avatar: avatar, //Retrieve avatar
              author: author.Name, //Retrieve author
              profile: profile, //Retrieve profile
              rating: rating, //Retrieve rating (only the number),;,
              title: title, //Retrieve title
              date: newDate(date), //Retrieve date
              helpful: helpful, //Retrieve helpful
              review: review, //Retrieve review
              reviewImg: reviewImg.Origin, //Retrieve reviewImg
              oldSystemFt: { //Retrieve old system features 
                Author: author,
                Engagement: engagement,
                Media: reviewImg,
                Unhelpful: unhelpful,
              },
            };
            dbo.collection("reviews_list").insertOne(myobj, function (err, res) { //sent to collection "reviews_list"
              if (err) throw err;
              console.log("Reviews inserted in Mongo");
              db.close();
            });
          })
        });
      }
    })
  } catch (err) {
    console.log(err);
  }
}

let resShow = async function () {

  var queueURL = "https://sqs.us-west-2.amazonaws.com/001431519265/reviewsOldSystem.fifo";

  var params = {
    AttributeNames: [
      "SentTimestamp"
    ],
    MaxNumberOfMessages: 1,
    MessageAttributeNames: [
      "All"
    ],
    QueueUrl: queueURL,
    VisibilityTimeout: 360,
    WaitTimeSeconds: 0
  };

  while (true) {
    await new Promise(resolve => setTimeout(resolve, 10000));
    sqs.receiveMessage(params, async function (err, data) { //Get the product id from the queue (amazonReviews)
      if (err) {
        console.log("Receive Error", err);
      } else if (data.Messages) {

        const myJSON = data.Messages[0].Body;
        const myObj = JSON.parse(myJSON);

        pdtID = myObj.product_ID;
        console.log("Message", pdtID);

        var MongoClient = require('mongodb').MongoClient;

        MongoClient.connect(mongoUrl, function (err, db) {
          if (err) throw err;
          var dbo = db.db("products");
          dbo.collection("products_list").findOne({ _id: ObjectId(`${myObj.product_ID}`) }, async function (err, result) { //Find the product in Mongo
            if (err) throw err;
            //retrieve product's id and name
            let pdt_ID1 = result._id;
            let pdt_ID = pdt_ID1.toString();
            console.log(result.PRODUCT_NAME);
            let pdt_name = result.PRODUCT_NAME;
            let normalized_pdt_name = normalize(pdt_name);
            await updateProductInfo(pdt_ID, normalized_pdt_name); //Update the product 
            await oldSystemReviews(pdt_ID, normalized_pdt_name); //Get the reviews

            db.close();
          });
        });

        var deleteParams = {
          QueueUrl: queueURL,
          ReceiptHandle: data.Messages[0].ReceiptHandle
        };
        sqs.deleteMessage(deleteParams, function (err, data) { //Delete the message in the queue after retrieving the product id
          if (err) {
            console.log("Delete Error", err);
          } else {
            console.log("Message Deleted", data);
          }
        });
      }
    });
  };
}

resShow();

module.exports = { updateProductInfo, oldSystemReviews, resShow };
