//Retrieve product information from WordPress website (With Config)

const puppeteer = require('puppeteer'); //Puppeteer connected
const $ = require('cheerio');
const reverseImageSearch = require('reverse-image-search-google');

const mongoose = require("mongoose"); //Mongo connected
const mongoUrl = "mongodb+srv://test:test@cluster0.mficn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority" //Mongo Link
mongoose.connect(mongoUrl, { //Connecting to Mongo
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err));

var AWS = require('aws-sdk'); //AWS connected
AWS.config.update({ region: 'us-west-2' });
var sqs = new AWS.SQS({ apiVersion: '2021-06-14' });

let testUrl = "https://michalnegrin.com/product/michelle-kitchen-towels-gold/"
// let testUrl = 'https://michalnegrin.com/product/michelle-kitchen-towels-gold/'; //Wordpress Url 
// let testUrl = 'https://weber.co.za/shop/accessories/lighter-cubes/'; //Wordpress Url
// let testUrl = `https://www.houseofwhiskyscotland.com/product/aberfeldy-15-year-old/` //Wordpress Url
// let testUrl = 'http://localhost:8000/product/wireless-audio-system-multiroom-360/';

const wordpressScrappingConfig = { //Config of all the selectors we need (Wordpress)
    "imgAttribute": "img.zoomImg",
    "titleAttribute": "h1.product_title.entry-title",
    "categoryAttribute": "span.posted_in > a[rel='tag']",
    "descAttribute": "div.woocommerce-product-details__short-description > p",
};

let getSrcALT = (config2, html, data, dataObj) => { //Retrieve src & alt
    $(config2.imgAttribute, html).each(function () {
        let src = $(this).attr('src')
        let alt = $(this).attr('alt')
        if (!alt) {
            alt = 'N/A' //If alt is empty
        }
        // console.log('src: ', src, 'alt: ', alt);
        data.push(src, alt);
        dataObj.src = src;
        dataObj.alt = alt;
    });
}

let getTitle = (config2, html, data, dataObj) => { //Retrieve title
    $(config2.titleAttribute, html).each(function () {
        let title = $(this).text();
        // console.log('Title: ', title);
        data.push(title);
        dataObj.title = title;
    });
}

let getDesc = (config2, html, data, dataObj) => { //Retrieve description
    $(config2.descAttribute, html).each(function () {
        let description = $(this).text();
        // console.log('Description: ', description);
        data.push(description)
        dataObj.description = description;
    });
}

let getCategory = (config2, html, data, dataObj) => { //Retrieve category (speaker, phone...)
    $(config2.categoryAttribute, html).each(function () {
        let category = $(this).text();
        // console.log('Category: ', category);
        data.push(category);
        dataObj.category = category;
    });
}

let scrapper = async function (url, myconfig) { //Scrapping the whole information

    let data = [];
    let dataObj = {};
    let res1 = await puppeteer
        .launch()
        .then(function (browser) {
            return browser.newPage();
        })
        .then(function (page) {
            return page.goto(url).then(function () {
                return page.content();
            });
        })
        .then(function (html) {
            getSrcALT(myconfig, html, data, dataObj);
            getTitle(myconfig, html, data, dataObj);
            if (getDesc) {
                getDesc(myconfig, html, data, dataObj);
            }
            if (getCategory) {
                getCategory(myconfig, html, data, dataObj);
            }
            return dataObj;
        })
        .catch(function (err) {
            console.log(err);
        });
    return res1;
}

let queues = [ //SQS Queues
    amazonQueue = "https://sqs.us-west-2.amazonaws.com/001431519265/reviewsAmazon.fifo", //Amazon
    oldSystemQueue = "https://sqs.us-west-2.amazonaws.com/001431519265/reviewsOldSystem.fifo", //OldSystem
]

let resShow = async function (url, config) {

    let result = await scrapper(url, config);

    console.log(result); //Returned by scrapper function 

    const doSomething = (results) => { //Trusted Name

        console.log(results); //Returned by reverse-img

        let trustedName;
        let trustedhosts = ['amazon']; //Trusted Websites 
        for (let i = 1; i < results.length - 1; i++) {
            if (results[i].url.includes(trustedhosts)) {
                trustedName = results[i].title;
                console.log('Trusted Name: ', trustedName); //Trusted name of the product
                break;
            } else {
                trustedName = result.title;
                console.log('Trusted Name: ', trustedName); //If no trusted hosts finded, use the title returned by scrapper function as a trusted name
                break;
            }
        }
        if (!result.src) {
            trustedName = result.title;
            console.log('Trusted Name: ', trustedName);
        }

        var MongoClient = require('mongodb').MongoClient;

        MongoClient.connect(mongoUrl, function (err, db) { //Send automatically the product (trusted) name to MongoDB
            if (err) throw err;
            var dbo = db.db("products"); //Connect to database "products"
            var myobj = {
                PRODUCT_NAME: trustedName,
            };
            dbo.collection("products_list").insertOne(myobj, function (err, res) { //sent to collection "products_list"
                if (err) throw err;

                console.log("Product name inserted in Mongo");
                db.close();
            });

            productID = myobj._id;
            productName = myobj.PRODUCT_NAME;

            for (let i = 0; i < queues.length; i++) {
                const params = { //Setup the sendMessage parameter object
                    MessageGroupId: '5',
                    MessageBody: JSON.stringify({
                        product_ID: productID,
                        date: (new Date()).toISOString(),
                    }),
                    QueueUrl: queues[i],
                };
                sqs.sendMessage(params, (err, data) => { //Send the product id to the queue (amazonReviews, oldSystemReviews)
                    if (err) {
                        console.log("Error", err);
                    } else {
                        console.log("Successfully added message", data.MessageId, "in the queue", i);
                        console.log(data);
                    }
                });
            }
        });

    }

    await new Promise(resolve => setTimeout(resolve, 1000));
    reverseImageSearch(result.src, doSomething)
    await new Promise(resolve => setTimeout(resolve, 1000));
};


let pixelScrapping = async function () {
    var queueURL = "https://sqs.us-west-2.amazonaws.com/001431519265/productResolver.fifo";

    var params = {
        AttributeNames: [
            "SentTimestamp"
        ],
        MaxNumberOfMessages: 1,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: queueURL,
        VisibilityTimeout: 360,
        WaitTimeSeconds: 0
    };

    while (true) {
        await new Promise(resolve => setTimeout(resolve, 60000));
        sqs.receiveMessage(params, async function (err, data) { //Get the product id from the queue (amazonReviews)
            if (err) {
                console.log("Receive Error", err);
            } else if (data.Messages) {

                const myJSON = data.Messages[0].Body;
                const myObj = JSON.parse(myJSON);

                let pdtURL = myObj.referrer;
                console.log("Message", pdtURL);
                resShow(pdtURL, wordpressScrappingConfig)

                var deleteParams = {
                    QueueUrl: queueURL,
                    ReceiptHandle: data.Messages[0].ReceiptHandle
                };
                sqs.deleteMessage(deleteParams, async function (err, data) { //Delete the message in the queue after retrieving the product id
                    if (err) {
                        console.log("Delete Error", err);
                    } else {
                        console.log("Message Deleted", data);
                    }
                });
            }
        });
    }
}

resShow(testUrl, wordpressScrappingConfig);
// pixelScrapping();

module.exports = { wordpressScrappingConfig, scrapper, resShow };


