//Searchs Product on Amazon and retrieves reviews of the first product (without Config) 

const puppeteer = require('puppeteer');
const $ = require('cheerio');

let doSearch = async function (searchQueries) { //Search Product
    searchQueries.forEach(searchQuery => {
        return searchGoogle(searchQuery);
    })
}

// let doSearchSrc = async function(src) {
//     let resultSrc = searchGoogle(src);
//     return resultSrc;
// }

// let doSearchAlt = async function(alt) {
//     let resultAlt = searchGoogle(alt); 
//     return resultAlt;
// }

// let doSearchTitle = async function(title) {
//     let resultTitle = searchGoogle(title);
//     return resultTitle;
// }

// let doSearchDesc = async function(description) {
//     let resultDesc = searchGoogle(description);
//     return resultDesc;
// }

// let doSearchCategory = async function(category) {
//     let resultCategory = searchGoogle(category);
//     return resultCategory;
// }

let searchGoogle = async (searchQuery) => {

    try {

        const browser = await puppeteer.launch({
            headless: false, //To see process in live (Chromium)
            defaultViewport: null,
            args: ["--no-sandbox"],
        });

        const page = await browser.newPage();

        await page.setUserAgent(`Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.49`);

        await page.goto('https://www.amazon.com'); //Amazon URL
        await page.type('input[name="field-keywords"]', searchQuery); //Search Bar
        await page.$eval('input[id="nav-search-submit-button"]', button => button.click()); //Search Button

        await page.waitForSelector('div[class="s-main-slot s-result-list s-search-results sg-row"]'); //Search Page
        await page.waitForTimeout(6000);
        await page.screenshot({ path: "screenShot1.png" });

        await page.$eval('span[class="a-size-base"]', button => button.click()); //First Product Reviews
        await page.waitForTimeout(8000);
        await page.screenshot({ path: "screenShot2.png" });

        await page.$eval('a[class="a-link-emphasis a-text-bold"]', button => button.click()); //Reviews Page
        await page.waitForTimeout(4000);
        await page.screenshot({ path: "screenShot3.png" });

        // await page.waitForSelector('div[id="a-page"]'); 
        // await page.waitForTimeout(4000);

        let index1 = 0;
        let searchResultArr = [];
        let searchResults;

        while (await page.waitForSelector('div[id="cm_cr-review_list"]') && index1 < 5) {

            searchResults = await page.$$eval('div[class="a-section review aok-relative"]', results => { //Review Section 

                let data = []; //Array to hold all our reviews

                results.forEach(result => { //Iterate over all the reviews

                    const avatar = result?.querySelector('div[class="a-profile-avatar"] > img')?.src; //Retrieve avatar 
                    const name = result?.querySelector('span[class="a-profile-name"]')?.innerText; //Retrieve name
                    const rating = result?.querySelector('span[class="a-icon-alt"]')?.innerText; //Retrieve rating
                    const title = result?.querySelector('a[class="a-size-base a-link-normal review-title a-color-base review-title-content a-text-bold"] > span')?.innerText; //Retrieve title
                    const date = result?.querySelector('span[class="a-size-base a-color-secondary review-date"]')?.innerText; //Retrieve date
                    const review = result?.querySelector('span[class="a-size-base review-text review-text-content"] > span')?.innerText; //Retrieve review
                    const helpful = result?.querySelector('span[class="a-size-base a-color-tertiary cr-vote-text"]')?.innerText; //Retrieve helpful section

                    data.push({ avatar, name, rating, title, date, helpful, review });
                });

                return data;

            });

            console.log(`---------------------------start of page ${index1 + 1}-------------------------------------------------------`);
            console.log(searchResults);
            console.log(`----------------------------end of page ${index1 + 1}--------------------------------------------------------`);

            searchResultArr[index1] = searchResults;
            index1++;

            await page.$eval('li[class="a-last"] > a', button => button.click()); //Next Page (Reviews)
            await page.waitForTimeout(4000);
            await page.screenshot({ path: "screenShot4.png" })
        }

        await browser.close();

        return searchResults;

        // let object = JSON.stringify(searchResults, null, 10);
        // console.log(object);

        // let linkerFunc = async function(url) {
        //     let trustedName = await firstSearch.scrapper(url);
        //     console.log(trustedName);
        // }
        // console.log(searchResults[0].url);
        // linkerFunc('http://localhost:8000/product/wireless-audio-system-multiroom-360/');

        // let trustedhosts=['amazon'];// = externalservice.get trutesdsorces
        // for (let i = 0; i < searchResults.length; i++) {
        //     if (searchResults[i].url.includes(trustedhosts)) {
        //         console.log('Trusted Name: ', searchResults[i].title);
        //     } 
        // }

    }
    catch (error) {
        console.log(error)
    }

};


doSearch(['airpods']); //Search reviews for product "airpods"
// doSearchSrc('https://images-na.ssl-images-amazon.com/images/I/61XYir5VEZL._AC_SL1500_.jpg');


module.exports = { doSearch, searchGoogle };