//Link between first search (product page) and second Search (search product on google)

var firstSearch = require('./productPageSearch.js'); //Scrapper
var secondSearch = require("./amazonSearchConfig.js"); //GoogleSearch
// Link with amazonSearchConfig too ??

let linkerFunc = async function (url) {

    try {

        let search1 = await firstSearch.resShow(url, firstSearch.wordpressScrappingConfig); //Sending URL to the scrapper
        let search2 = await secondSearch.doSearch({ alt: search1.alt, src: search1.src, title: search1.title }, secondSearch.amazonSearchConfig) //Receiving the results from google search (with alt/src/title)

        return { search2 };

    }
    catch (error) {

        console.log(error);
        return error;

    }

};

linkerFunc('http://localhost:8000/product/wireless-audio-system-multiroom-360/'); //Sending the URL of the product page to the scrapper in order to start the whole process
// linkerFunc('http://localhost:8081/2/3/referer');

module.exports = { linkerFunc }