//Same stuff than linker.js but running it locally (port 3000)

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
const{ linkerFunc }  = require('./Modules/linker');

app.get('/', async function (req, res, next) {
  res.send('Hello World')
});

app.post('/', function(req, res, next) {
  console.log(req.body.url);
    // console.log(linkerFunc(req.body.url)); //Linker Execution
    res.send('Post Method') 
});
 
app.listen(3000, function() {
    console.log('Started on port: 3000')
});